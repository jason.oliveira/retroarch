# RetroArch

This is a temporary repo so I can dogfood changes to the retroarch ebuilds before submission back into gentoo proper.

# Cores
By default, the retroarch ebuild disables USE="cores", and does not build any of them. Because you can just download them through retroarch, and, much like lutris' binaries, the cores will live in ~/.local/retroarch or ~/.config. If you think this is braindamaged, please help me by contributing ebuilds for the cores again. I took them out because I hate them. 

# Adding This Repo

### How to add this repo using eselect-repository:    

`eselect repository add retroarch git https://gitlab.com/jason.oliveira/retroarch.git`

### How to add this repo in Layman:    

`sudo layman -o https://gitlab.com/jason.oliveira/retroarch/raw/master/repositories.xml -f -a retroarch`

