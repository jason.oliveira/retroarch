# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
LIBRETRO_COMMIT_SHA="977612e2cd284f67fc0d121d9d94c5982a49f61e"
LIBRETRO_DATA_DIR="${EROOT}usr/share/libretro"
RETROARCH_DATA_DIR="${EROOT}usr/share/retroarch"
: ${LIBRETRO_REPO_NAME:="libretro/${PN}"}
if [[ ${PV} = 9999 ]]; then
        inherit git-r3
        EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
fi
if [[ ! ${PV} = 9999 ]] && [[ ! ${PN} = retroarch ]]; then
        [ ${LIBRETRO_COMMIT_SHA} = die ] && die "LIBRETRO_COMMIT_SHA must be set before inherit."
        SRC_URI="https://github.com/${LIBRETRO_REPO_NAME}/archive/${LIBRETRO_COMMIT_SHA}.tar.gz -> ${P}.tar.gz"
        RESTRICT="primaryuri"
        S="${WORKDIR}/${LIBRETRO_REPO_NAME##*/}-${LIBRETRO_COMMIT_SHA}"
fi

DESCRIPTION="Repository containing cheatcode files, content data files, etc."
HOMEPAGE="https://github.com/libretro/libretro-database"
KEYWORDS="amd64 x86 arm"

LICENSE="GPL-3"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}"

src_install() {
	dodir "${LIBRETRO_DATA_DIR}"/cheats/
	cp -R "${S}"/cht/* "${D}${LIBRETRO_DATA_DIR}"/cheats/
	dodir "${LIBRETRO_DATA_DIR}"/data/
	cp -R "${S}"/dat/* "${D}${LIBRETRO_DATA_DIR}"/data/
	cp -R "${S}"/rdb/* "${D}${LIBRETRO_DATA_DIR}"/data/
}
