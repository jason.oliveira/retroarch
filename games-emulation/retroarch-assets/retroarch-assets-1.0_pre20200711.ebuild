# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LIBRETRO_COMMIT_SHA="33f26cea64280aad73d5ff5336afcfc1f8ff5a0a"
LIBRETRO_DATA_DIR="${EROOT}usr/share/libretro"
RETROARCH_DATA_DIR="${EROOT}usr/share/retroarch"
: ${LIBRETRO_REPO_NAME:="libretro/${PN}"}
if [[ ${PV} = 9999 ]]; then
        inherit git-r3
        EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
fi
if [[ ! ${PV} = 9999 ]] && [[ ! ${PN} = retroarch ]]; then
        [ ${LIBRETRO_COMMIT_SHA} = die ] && die "LIBRETRO_COMMIT_SHA must be set before inherit."
        SRC_URI="https://github.com/${LIBRETRO_REPO_NAME}/archive/${LIBRETRO_COMMIT_SHA}.tar.gz -> ${P}.tar.gz"
        RESTRICT="primaryuri"
        S="${WORKDIR}/${LIBRETRO_REPO_NAME##*/}-${LIBRETRO_COMMIT_SHA}"
fi


DESCRIPTION="RetroArch Assets files"
HOMEPAGE="https://github.com/libretro/retroarch-assets"
KEYWORDS="amd64 x86 arm"

LICENSE="GPL-3"
SLOT="0"

RDEPEND=""
DEPEND="${RDEPEND}"

src_install() {
	dodir "${RETROARCH_DATA_DIR}"/assets
	cp -R "${S}"/* "${D}${RETROARCH_DATA_DIR}"/assets
}
