# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# Some stuff yanked-in from the eclass - needs cleanup.
LIBRETRO_DATA_DIR="${EROOT}usr/share/libretro"
RETROARCH_DATA_DIR="${EROOT}usr/share/retroarch"
: ${LIBRETRO_COMMIT_SHA:=die} #}
: ${LIBRETRO_REPO_NAME:="libretro/${PN}"}
EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
# inherit libretro-core flag-o-matic python-single-r1

inherit libretro-core

DESCRIPTION="RetroArch joypad autoconfig files"
HOMEPAGE="https://github.com/libretro/retroarch-joypad-autoconfig"
KEYWORDS="~amd64 ~x86 ~arm"

LICENSE="GPL-3"
SLOT="0"

RDEPEND="virtual/udev"
DEPEND="${RDEPEND}"

src_install() {
	insinto "${RETROARCH_DATA_DIR}"/autoconfig
	doins "${S}"/udev/*.cfg
}
