# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
EAPI=7

LIBRETRO_CORE_NAME="2048"
LIBRETRO_REPO_NAME="libretro/libretro-${PN//-libretro}"
LIBRETRO_COMMIT_SHA="1ff7d5c3835ad89e5ce5eaa409b91c9a17cc1aa0"
EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
inherit libretro-core git-r3

DESCRIPTION="libretro implementation of 2048. (Puzzle game)"
HOMEPAGE="https://github.com/libretro/libretro-2048"

KEYWORDS="amd64 x86 arm"
LICENSE="GPL-2"
SLOT="0"

DEPEND="media-libs/freetype
		media-libs/fontconfig"
RDEPEND="${DEPEND}
		games-emulation/libretro-info"
src_prepare(){
		${S}=${P}
}
