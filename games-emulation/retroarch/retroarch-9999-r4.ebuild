# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# TODO: Rewrite src_prepare() and src_configure()

EAPI=7

PYTHON_COMPAT=( python{3_8,3_9,3_10,3_11} )

LIBRETRO_REPO_NAME="libretro/RetroArch"

DESCRIPTION="Universal frontend for libretro-based emulators"
HOMEPAGE="http://www.retroarch.com"

# Some stuff yanked-in from the eclass - needs cleanup.
LIBRETRO_DATA_DIR="${EROOT}usr/share/libretro"
RETROARCH_DATA_DIR="${EROOT}usr/share/retroarch"
: ${LIBRETRO_COMMIT_SHA:=die} #}
: ${LIBRETRO_REPO_NAME:="libretro/${PN}"}
EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
inherit libretro-core flag-o-matic python-single-r1


if [[ ${PV} = 9999 ]]; then
	# Inherit and EGIT_REPO_URI already set by eclass
	inherit git-r3
    # EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
	SRC_URI=""
	KEYWORDS="~amd64 ~x86 ~arm"
else
	SRC_URI="https://github.com/${LIBRETRO_REPO_NAME}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	RESTRICT="primaryuri"
	S="${WORKDIR}/RetroArch-${PV}"
	KEYWORDS="amd64 x86 arm"
fi

LICENSE="GPL-3"
SLOT="0"

#FIXME: Revaluate the "wayland? ( egl )" entry below. Due to unresolved upstream
#issues, Wayland support erroneously requires EGL support. Ideally, it
#shouldn't. When upstream resolves this, remove this entry. See also:
#    https://github.com/stefan-gr/abendbrot/issues/7#issuecomment-204541979

IUSE="+7zip alsa +armvfp assets bearssl blissbox +bluetooth cg +cheevos cores database dbus debug discord dispmanx ffmpeg flac gles2 gles3 +hid jack joypad_autoconfig kms lakka libass libcaca libdecor libsixel +libusb materialui mbedtls +mmap mpv neon +network networkvideo networkgamepad openal +opengl openvg osmesa oss overlays pulseaudio qt5 sdl +sdl2 shaders +slang +spirv +ssl systemd +truetype +threads +udev +userspace v4l2 videocore vulkan wayland X xinerama +xmb +xml +xrandr +xv zlib cpu_flags_x86_sse2 python"

REQUIRED_USE="
	alsa? ( threads )
	arm? ( gles2 )
	!arm? (
		gles2? ( opengl )
		xmb? ( opengl )
	)
	bearssl? ( !mbedtls )
	cg? ( opengl )
	dispmanx? ( videocore arm )
	gles2? ( !cg )
	gles3? ( gles2 )
	lakka? ( assets cores database joypad_autoconfig overlays shaders xmb )
	libass? ( ffmpeg )
	libusb? ( hid )
	mbedtls? ( !bearssl )
	networkvideo? ( networkgamepad )
	openvg? ( opengl )
	python? ( ${PYTHON_REQUIRED_USE} )
	ssl? ( ?? ( mbedtls bearssl ) )
	userspace? ( !assets !cores !database !joypad_autoconfig !overlays !shaders )
	videocore? ( arm )
	vulkan? ( amd64 )
	xinerama? ( X )
	xv? ( X )
"
# Disabled above:
#	xmb? ( assets )
#    sdl2? ( !sdl )
#    sdl? ( !sdl2 )
#    ?? ( sdl sdl2 )

# These are disabled due to dev request:
# <bparker> also it's entirely valid to be able to run retroarch without any video driver
#  available (set video_driver to "null")
# <bparker> for headless tasks
# <bparker> e.g. some people have it run a game for X frames, take a screenshot and then quit
# <bparker> to automate testing
# || ( kms X wayland videocore )
# || ( alsa jack openal oss pulseaudio )
# || ( opengl sdl sdl2 vulkan dispmanx )

RDEPEND="
	alsa? ( media-libs/alsa-lib:0= )
	arm? ( dispmanx? ( || ( media-libs/raspberrypi-userland:0 media-libs/raspberrypi-userland-bin:0 ) ) )
	assets? ( games-emulation/retroarch-assets:0= )
	bluetooth? ( net-wireless/bluez:0= )
	cg? ( media-gfx/nvidia-cg-toolkit:0= )
	cores? ( games-emulation/libretro-meta:0= )
	database? ( games-emulation/libretro-database:0= )
	dbus? ( sys-apps/dbus:0= )
	discord? ( net-im/discord-bin:0= )
	ffmpeg? ( >=media-video/ffmpeg-2.1.3:0= )
	flac? ( media-libs/flac:0= )
	hid? ( dev-libs/hidapi:0= )
	jack? ( virtual/jack:= )
	joypad_autoconfig? ( games-emulation/retroarch-joypad-autoconfig:0= )
	libass? ( media-libs/libass:0= )
	libcaca? ( media-libs/libcaca:0= )
	libdecor? ( gui-libs/libdecor:= )
	libsixel? ( media-libs/libsixel:0= )
	libusb? ( virtual/libusb:1= )
	mpv? ( media-video/mpv:0= )
	openal? ( media-libs/openal:0= )
	opengl? ( media-libs/mesa:0=[gles2?] )
	openvg? ( media-libs/mesa:0= )
	osmesa? ( media-libs/mesa:0=[osmesa?] )
	overlays? ( games-emulation/common-overlays:0= )
	pulseaudio? ( media-sound/pulseaudio:0= )
	python? ( ${PYTHON_DEPS} )
	qt5? (
			dev-qt/qtcore:5
			dev-qt/qtgui:5
			dev-qt/qtopengl:5
			dev-qt/qtwidgets:5
	)
	sdl? ( <=media-libs/libsdl-1.2.20:0=[joystick] )
	sdl2? ( media-libs/libsdl2:0=[joystick] )
	shaders? ( vulkan? ( games-emulation/slang-shaders ) )
	slang? ( dev-util/glslang )
	spirv? (
			dev-util/spirv-tools
			dev-util/spirv-headers
	)
	ssl? ( !mbedtls? ( net-libs/mbedtls:0= ) )
	systemd? ( sys-apps/systemd )
	truetype? ( media-libs/freetype:2= )
	udev? ( virtual/udev:0=
		X? ( x11-drivers/xf86-input-evdev:0= )
	)
	amd64? ( vulkan? ( media-libs/vulkan-loader:0= ) )
	v4l2? ( media-libs/libv4l:0= )
	wayland? ( media-libs/mesa:0=[wayland?] )
	X? (
		x11-base/xorg-server:0=
		>=x11-libs/libxkbcommon-0.4.0:0=
	)
	xinerama? ( x11-libs/libXinerama:0= )
	xml? ( dev-libs/libxml2:2= )
	xrandr? ( x11-libs/libXrandr:= )
	xv? ( x11-libs/libXv:= )
	zlib? ( sys-libs/zlib:0= )
"
DEPEND="${RDEPEND}
	virtual/pkgconfig
"
# used to be glsl-shaders and common-shaders.
# This is no longer needed, so will likely be removed/ changed in the future.
PDEPEND="!vulkan? ( shaders? ( games-emulation/slang-shaders:0= ) )
			!vulkan? ( cg? ( games-emulation/slang-shaders:0= ) )
"

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_prepare() {
	# If Python support is enabled, use the currently enabled "python" binary.
	if use python; then
		sed -i qb/config.libs.sh \
			-e "s:%PYTHON_VER%:${EPYTHON/python/}:" \
			|| die '"sed" failed.'
	fi

	# Install application data and pixmaps to standard directories.
	sed -i Makefile \
		-e 's:$(DESTDIR)$(PREFIX)/share/\(applications\|pixmaps\):$(DESTDIR)usr/share/\1:' \
		|| die '"sed" failed.'

	# Absolute path of the directory containing Retroarch shared libraries.
	export RETROARCH_LIB_DIR="${EROOT}usr/$(get_libdir)/retroarch"

	# Absolute path of the directory containing Libretro shared libraries.
	export LIBRETRO_LIB_DIR="${EROOT}usr/$(get_libdir)/libretro"

	# Replace stock defaults with Gentoo-specific defaults, unless userspace.
	if use userspace; then
		sed -i retroarch.cfg \
            -e 's:# \(libretro_info_path =\):\1 "~/.config/retroarch/info/":' \
            -e 's:# \(joypad_autoconfig_dir =\):\1 "~/.config/retroarch/autoconfig/":' \
            -e 's:# \(assets_directory =\):\1 "~/.config/retroarch/assets/":' \
            -e 's:# \(rgui_config_directory =\):\1 "~/.config/retroarch/":' \
            -e 's:# \(libretro_directory =\):\1 "~/.config/retroarch/cores/":' \
            -e 's:# \(video_filter_dir =\):\1 "~/.config/retroarch/filters/video/":' \
            -e 's:# \(audio_filter_dir =\):\1 "~/.config/retroarch/filters/audio/":' \
            -e 's:# \(overlay_directory =\):\1 "~/.config/retroarch/overlays/":' \
            -e 's:# \(content_database_path =\):\1 "~/.config/retroarch/data/":' \
            -e 's:# \(cheat_database_path =\):\1 "~/.config/retroarch/cheats/":' \
            -e 's:# \(system_directory =\):\1 "~/.local/share/retroarch/system/":' \
            -e 's:# \(savestate_directory =\):\1 "~/.local/share/retroarch/savestates/":' \
            -e 's:# \(savefile_directory =\):\1 "~/.local/share/retroarch/savefiles/":' \
            -e 's:# \(screenshot_directory =\):\1 "~/.local/share/retroarch/screenshots/":' \
            -e 's:# \(extraction_directory =\):\1 "'${EROOT}'tmp/":' \
            -e 's:# \(menu_show_core_updater =\):\1 "true":' \
            -e 's:# \(content_directory =\):\1 "~/":' \
            -e 's:# \(rgui_browser_directory =\):\1 "~/":' \
            || die '"sed" failed.'
	else
		sed -i retroarch.cfg \
			-e 's:# \(libretro_directory =\):\1 "'${LIBRETRO_LIB_DIR}'/":' \
			-e 's:# \(libretro_info_path =\):\1 "'${LIBRETRO_DATA_DIR}'/info/":' \
			-e 's:# \(joypad_autoconfig_dir =\):\1 "'${RETROARCH_DATA_DIR}'/autoconfig/":' \
			-e 's:# \(assets_directory =\):\1 "'${RETROARCH_DATA_DIR}'/assets/":' \
			-e 's:# \(rgui_config_directory =\):\1 "~/.config/retroarch/":' \
			-e 's:# \(video_filter_dir =\):\1 "'${RETROARCH_LIB_DIR}'/filters/video/":' \
			-e 's:# \(audio_filter_dir =\):\1 "'${RETROARCH_LIB_DIR}'/filters/audio/":' \
			-e 's:# \(overlay_directory =\):\1 "'${LIBRETRO_DATA_DIR}'/overlays/":' \
			-e 's:# \(content_database_path =\):\1 "'${LIBRETRO_DATA_DIR}'/data/":' \
			-e 's:# \(cheat_database_path =\):\1 "'${LIBRETRO_DATA_DIR}'/cheats/":' \
			-e 's:# \(system_directory =\):\1 "~/.local/share/retroarch/system/":' \
			-e 's:# \(savestate_directory =\):\1 "~/.local/share/retroarch/savestates/":' \
			-e 's:# \(savefile_directory =\):\1 "~/.local/share/retroarch/savefiles/":' \
			-e 's:# \(screenshot_directory =\):\1 "~/.local/share/retroarch/screenshots/":' \
			-e 's:# \(extraction_directory =\):\1 "'${EROOT}'tmp/":' \
			-e 's:# \(menu_show_core_updater =\):\1 "true":' \
			-e 's:# \(content_directory =\):\1 "~/":' \
			-e 's:# \(rgui_browser_directory =\):\1 "~/":' \
			|| die '"sed" failed.'
	fi
# cores disabled until all are resubmitted upstream. 
# Download cores from inside RetroArch until then.
#	if use cores; then
#		sed -i retroarch.cfg \
#			-e 's:# \(menu_show_core_updater =\) true:\1 "false":'
#	fi

	if use vulkan; then
		if use userspace; then
			sed -i retroarch.cfg \
				-e 's:# \(video_shader_dir =\):\1 "~/.config/retroarch/slang-shaders/":' \
                || die '"sed failed.'
		else
			sed -i retroarch.cfg \
				-e 's:# \(video_shader_dir =\):\1 "'${LIBRETRO_DATA_DIR}'/slang-shaders/":' \
				|| die '"sed failed.'
		fi
	else
		if use userspace; then
			use cg && sed -i retroarch.cfg \
                -e 's:# \(video_shader_dir =\):\1 "~/.config/retroarch/common-shaders/":'
            use cg || use shaders && sed -i retroarch.cfg \
                -e 's:# \(video_shader_dir =\):\1 "~/.config/retroarch/shaders/":'
		else
			use cg && sed -i retroarch.cfg \
				-e 's:# \(video_shader_dir =\):\1 "'${LIBRETRO_DATA_DIR}'/common-shaders/":'
			use cg || use shaders && sed -i retroarch.cfg \
				-e 's:# \(video_shader_dir =\):\1 "'${LIBRETRO_DATA_DIR}'/shaders/":'
		fi
	fi

	default_src_prepare
}

src_configure() {
	filter-flags --infodir

	if use cg; then
		append-ldflags -L"${EROOT}"opt/nvidia-cg-toolkit/$(get_libdir)
		append-cflags  -I"${EROOT}"opt/nvidia-cg-toolkit/include
	fi

	if use videocore; then
		export HAVE_VIDEOCORE="yes"
	else
		export HAVE_VIDEOCORE="no"
		sed -i qb/config.libs.sh \
			-e 's:\[ -d /opt/vc/lib \] && add_library_dirs /opt/vc/lib && add_library_dirs /opt/vc/lib/GL::' || die 'sed failed'
	fi

	if use lakka; then
		export HAVE_LAKKA="1"
	fi
	if use qt5; then
		PATH="$PATH:/usr/lib64/qt5/bin"
	fi

	# Microphone Support
	if use sdl || sdl2 || alsa; then
		export HAVE_MICROPHONE="1"
	fi

#	# Note that OpenVG support is hard-disabled. (See ${RDEPEND} above.)
	# mpv is also not enabled by default, it doesn't seem to work with stable mpv.
	./configure \
		$(use_enable 7zip) \
		$(use_enable alsa) \
		$(use_enable bearssl builtinbearssl) \
		$(use_enable blissbox) \
		$(use_enable bluetooth) \
		$(use_enable cheevos) \
		$(use_enable cg) \
		$(use_enable cpu_flags_x86_sse2 sse) \
		$(use_enable dbus) \
		$(use_enable discord) \
		$(use_enable dispmanx) \
		$(use_enable ffmpeg) \
		$(use_enable flac) \
		$(use_enable gles2 opengles) \
		$(use_enable gles3 opengles3) \
		$(use_enable hid) \
		$(use_enable jack) \
		$(use_enable kms) \
		$(use_enable libass ssa) \
		$(use_enable libcaca caca) \
		$(use_enable libdecor) \
		$(use_enable libusb) \
		$(use_enable materialui) \
		$(use_enable mbedtls builtinmbedtls) \
		$(use_enable mmap) \
		$(use_enable mpv) \
		$(use_enable networkgamepad) \
		$(use_enable networkvideo network_video) \
		$(use_enable network networking) \
		$(use_enable neon) \
		$(use_enable openal al) \
		$(use_enable opengl) \
		$(use_enable openvg vg) \
		$(use_enable osmesa) \
		$(use_enable oss) \
		$(use_enable pulseaudio pulse) \
		$(use_enable qt5 qt) \
		$(use_enable sdl) \
		$(use_enable sdl2) \
		$(use_enable slang glslang) \
		$(use_enable libsixel sixel) \
		$(use_enable spirv spirv_cross) \
		$(use_enable ssl) \
		$(use_enable systemd) \
		$(use_enable truetype freetype) \
		$(use_enable udev) \
		$(use_enable v4l2) \
		$(use_enable vulkan) \
		$(use_enable wayland) \
		$(use_enable X x11) \
		$(use_enable xinerama) \
		$(use_enable xmb) \
		$(use_enable xrandr) \
		$(use_enable xv xvideo) \
		$(use_enable zlib) \
		--enable-dynamic \
		--disable-vg \
		|| die "configure failed."
}

src_compile() {
	# Filtering all -O* flags in favor of upstream ones
	filter-flags -O*
	emake $(usex debug "DEBUG=1" "")
	emake $(usex debug "build=debug" "build=release") -C gfx/video_filters/
	emake $(usex debug "build=debug" "build=release") -C libretro-common/audio/dsp_filters/
}

src_install() {
	# Install core files and directories.
	emake DESTDIR="${ED}" install

	# Install documentation.
	dodoc README.md

	# Install video filters.
	insinto ${RETROARCH_LIB_DIR}/filters/video/
	doins "${S}"/gfx/video_filters/*.so
	doins "${S}"/gfx/video_filters/*.filt

	# Install audio filters.
	insinto ${RETROARCH_LIB_DIR}/filters/audio/
	#doins "${S}"/audio/audio_filters/*.so
	doins "${S}"/libretro-common/audio/dsp_filters/*.dsp

	# Preserve empty directories.
	keepdir ${LIBRETRO_LIB_DIR}
	keepdir ${LIBRETRO_DATA_DIR}/info/
	use vulkan || keepdir ${LIBRETRO_DATA_DIR}/shaders/
	use vulkan && keepdir ${LIBRETRO_DATA_DIR}/slang-shaders/
	keepdir ${LIBRETRO_DATA_DIR}/overlays/
	keepdir ${LIBRETRO_DATA_DIR}/cheats/
	keepdir ${LIBRETRO_DATA_DIR}/data/
	keepdir ${RETROARCH_DATA_DIR}/assets/
	keepdir ${RETROARCH_DATA_DIR}/autoconfig/
}

pkg_preinst() {
	if ! has_version "<=${CATEGORY}/${PN}-${PVR}" ; then
		first_install="1"
	else
		if [[ "${PV}" < "9999" ]]; then
			if has_version "<${CATEGORY}/${PN}-${PVR}"; then
				upgrade_info="1"
			fi
		else
			if has_version "<${CATEGORY}/${PN}-9999-r2" ; then
				upgrade_info="1"
			fi
		fi
	fi
}

# Please note that pkg_postinst() needs considerable reworking:
# Todo: 
# RetroArch needs a base configuration change to point at
# a user's home directory if bindist useflag is enabled.

pkg_postinst() {
	if use userspace; then
		elog "Things will look a little strange upon first bootup. You are encouraged to"
		elog "go to Main Menu -> Online Updater -> Update (Core Info Files/Assets/Controller Profiles/etc)."
		elog "(you may have to use a keyboard to do this initially)"
		elog "RetroArch will look normal once this is done."
		elog ""
	fi
	if [[ "${upgrade_info}" == "1" ]]; then
		elog ""
		elog "You need to make sure that all directories exist or you must modify your retroarch.cfg accordingly."
		elog "To create the needed directories for your user run as \$USER (not as root!):"
		elog ""
		elog "\$ mkdir -p ~/.local/share/retroarch/{savestates,savefiles,screenshots,system}"
		elog ""
	fi
	if [[ "${upgrade_info}" == "1" ]]; then
		ewarn ""
		ewarn "The path to cores and datafiles changed to [/usr/lib64/libretro, /usr/share/libretro] due to EAPI 6 update."
		ewarn "This can be changed via gui or cfg file found in \"\${HOME}/.config/retroarch/retroarch.cfg\""
		ewarn ""
	fi
}
