# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7
# Some stuff yanked-in from the eclass - needs cleanup.
#LIBRETRO_DATA_DIR="${EROOT}usr/share/libretro"
#RETROARCH_DATA_DIR="${EROOT}usr/share/retroarch"
#: ${LIBRETRO_COMMIT_SHA:=die} #}
#: ${LIBRETRO_REPO_NAME:="libretro/${PN}"}
EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
LIBRETRO_REPO_NAME="libretro/${PN}"
inherit libretro-core


DESCRIPTION="Collection of overlay files for use with libretro frontends."
HOMEPAGE="https://github.com/libretro/common-overlays"
KEYWORDS=""

LICENSE="GPL-3"
SLOT="0"

RDEPEND=""
DEPEND="${RDEPEND}"

src_install() {
	dodir "${LIBRETRO_DATA_DIR}"/overlays
	cp -R "${S}"/* "${D}${LIBRETRO_DATA_DIR}"/overlays
}
