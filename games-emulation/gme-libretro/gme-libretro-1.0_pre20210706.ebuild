# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
LIBRETRO_REPO_NAME="libretro/libretro-gme"
EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
LIBRETRO_COMMIT_SHA="40d8b3bf4f0bd4f713f65e08c62d30b1ae8b2282"
inherit libretro-core

DESCRIPTION="Libretro implementation of Game Music Emulator (Chiptune and VG Music)"
HOMEPAGE="https://github.com/libretro/libretro-gme"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"

DEPEND=""
RDEPEND="${DEPEND}
				games-emulation/libretro-info"
BDEPEND=""
