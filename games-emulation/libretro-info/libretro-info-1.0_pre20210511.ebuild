# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

LIBRETRO_REPO_NAME="libretro/libretro-super"
LIBRETRO_COMMIT_SHA="674ea82348b9e606bf527477dd540b863f4d3c27"
LIBRETRO_DATA_DIR="${EROOT}usr/share/libretro"
RETROARCH_DATA_DIR="${EROOT}usr/share/retroarch"
if [[ ${PV} = 9999 ]]; then
        inherit git-r3
        EGIT_REPO_URI="https://github.com/${LIBRETRO_REPO_NAME}.git"
fi
if [[ ! ${PV} = 9999 ]] && [[ ! ${PN} = retroarch ]]; then
        [ ${LIBRETRO_COMMIT_SHA} = die ] && die "LIBRETRO_COMMIT_SHA must be set before inherit."
        SRC_URI="https://github.com/${LIBRETRO_REPO_NAME}/archive/${LIBRETRO_COMMIT_SHA}.tar.gz -> ${P}.tar.gz"
        RESTRICT="primaryuri"
        S="${WORKDIR}/${LIBRETRO_REPO_NAME##*/}-${LIBRETRO_COMMIT_SHA}"
fi

DESCRIPTION="Libretro info files required for libretro cores"
HOMEPAGE="https://github.com/libretro/libretro-super"
KEYWORDS="amd64 x86 arm"

LICENSE="GPL-3"
SLOT="0"

DEPEND=""
RDEPEND="${DEPEND}"

src_compile() {
	:
}

src_install() {
	insinto "${LIBRETRO_DATA_DIR}"/info
	doins dist/info/*.info
}
